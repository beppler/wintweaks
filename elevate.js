var args = WScript.Arguments;

if (args.length < 1) {
    WScript.Echo("Usage: elevate.js program [parameters...]");
    WScript.Quit(1);    
}

var program = args.Item(0); 

var params = "";
for (var i = 1; i < args.length; i++)
    params += " \"" + args.Item(i).replace("\"", "\"\"") + "\"";

var application = WScript.CreateObject("Shell.Application"); 
application.ShellExecute(program, params, "", "runas", 1);

WScript.Quit(0);